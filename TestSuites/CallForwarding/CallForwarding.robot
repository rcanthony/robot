*** Settings ***
Suite Setup	MyAmaysim Call Forwarding Setup
Suite Teardown	Close Browser
Resource	../../Resources/Keywords.tsv

*** Test Cases ***
Verify Call Forwarding Default Elements
	Verify Elements Are Visible	@{CallForwarding-DefaultElements}

Alert message should prompt as user clicks edit on Call Forwarding
	Click	${CallForwading-Edit}
	Verify Elements Are Visible	@{CallForwarding-EditAlert}
	Verify Page Contain Texts	${CallForwardingEditAlertMessage}	${CallForwardingEditConfirmationMessage}
	[Teardown]	Manage Call Forwarding Settings Teardown

User should be able to proceed to edit mode when confirm is selected
	[Setup]	Manage Call Forwading Settings Setup
	Verify Elements Are Visible	@{CallForwardingEditMode-DefaultElements}
	[Teardown]	Manage Call Forwarding Settings Teardown

Changes should not be saved when user selects cancel
	[Setup]	Manage Call Forwading Settings Setup
	Click	${CallForwarding-RadioNo}
	Click	${Cancel}
	Wait Until Element Is Not Visible	${LoadingSpinner}	${Timeout}
	Verify Elements Are Visible	${AllowCallForwarding-LabelYes}

Forward calls should not be displayed when Allow Call Forwarding is set to No
	[Setup]	Manage Call Forwading Settings Setup
	Click	${CallForwarding-RadioNo}
	Verify Elements Are Not Visible	@{ForwardToElements}
	[Teardown]	Manage Call Forwarding Settings Teardown

Forward calls should be displayed when allow call forwarding is set to Yes
	[Setup]	No Allow Call Forwarding Setup
	Click	${CallForwarding-RadioYes}
	Verify Elements Are Visible	@{CallForwardingEditMode-DefaultElements}[1]	${ForwardCall-TextField}
	[Teardown]	Manage Call Forwarding Settings Teardown

Changes on Forward calls should persist when user selects save
	[Setup]	Manage Call Forwading Settings Setup
	Input Text	${ForwardCall-TextField}	@{TestAustralianNumber}[1]
	Click	${Save}
	Wait Until Element Is Not Visible	${LoadingSpinner}	${Timeout}
	Wait Until Page Contains Element	${CallForwading-CloseMessage}	${Timeout}
	Click	${CallForwading-CloseMessage}
	Verify Page Contain Texts	@{TestAustralianNumber}[1]
	[Teardown]	Changes On Forward Call Teardown

Changes on Allow Call Forwarding should persist when user selects save
	[Setup]	Manage Call Forwading Settings Setup
	Click	${CallForwarding-RadioNo}
	Click	${Save}
	Wait Until Element Is Not Visible	${LoadingSpinner}	${Timeout}
	Wait Until Page Contains Element	${CallForwading-CloseMessage}	${Timeout}
	Click	${CallForwading-CloseMessage}
	Verify Elements Are Visible	${AllowCallForwarding-LabelNo}
	[Teardown]	Changes On Allow Call Forwarding Teardown

User should not be able to proceed to edit mode when cancel is selected
	Click	${CallForwading-Edit}
	Wait Until Page Contains Element	${AlertCancel}	${Timeout}
	Click	${AlertCancel}
	Verify Elements Are Not Visible	@{CallForwardingEditMode-DefaultElements}

Alert message should prompt on successful update of settings
	[Setup]	Manage Call Forwading Settings Setup
	Input Text	${ForwardCall-TextField}	@{TestAustralianNumber}[1]
	Click	${Save}
	Wait Until Element Is Not Visible	${LoadingSpinner}	${Timeout}
	Verify Page Contain Texts	${SuccessfulUpdateMessage}
	[Teardown]	Run Keywords  	Click	${CallForwading-CloseMessage}	AND	Changes On Forward Call Teardown