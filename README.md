## Setup for windows
	Download Chromedriver on https://chromedriver.storage.googleapis.com/index.html?path=74.0.3729.6/
	Download Python on https://www.python.org/ftp/python/3.7.3/python-3.7.3-amd64.exe
	Download chromedriver http://chromedriver.chromium.org/


** Add the Python, Pip and chromedriver to the Environmental Variables 'PATH'**  
	Python: PythonDirectory\Python37  
	Pip: PythonDirectory\Python37\Scripts  
	Chromedriver: ChromedriverDirectory\  

** Installing Robotframework **  
pip install robotframework  
pip install robotframework-selenium2library  

** Check if the variables are properly set **  
	on bash/terminal, try  
		python --version  
		pip --version  
		robot --version  
		chromedriver --version  
	
	also take note that chromedriver version should be the same with the version of chrome
	
** To run or execute a test **  
	on bash/terminal, try  
		'robot RepositoryDirectory/testsuite.robot'  

** Guides on robotframework keywords: **  
	http://robotframework.org/robotframework/latest/libraries/BuiltIn.html  
	http://robotframework.org/Selenium2Library/Selenium2Library.html  
	https://robotframework.org/robotframework/latest/libraries/String.html  